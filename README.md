# Spring Boot CSV Upload

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/springboot-csv-upload.git`
2. Navigate to the folder: `cd springboot-csv-upload`
3. Run the application: `mvn clean spring-boot:run`
4. Open POSTMAN APP then hit the API:

* Upload: `POST | localhost:8080/books/upload`
* Get all books: `GET | localhost:8080/books` 
