package com.hendisantika.csvupload.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-csv-upload
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/06/21
 * Time: 11.44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseData {
    private boolean status;
    private List<String> messages = new ArrayList<>();
    private Object payload;
}
