package com.hendisantika.csvupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootCsvUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootCsvUploadApplication.class, args);
    }

}
