package com.hendisantika.csvupload.service;

import com.hendisantika.csvupload.entity.Book;
import com.hendisantika.csvupload.helper.CSVHelper;
import com.hendisantika.csvupload.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-csv-upload
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/06/21
 * Time: 11.47
 */
@Service("bookService")
@Transactional
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    public List<Book> save(MultipartFile file) {
        try {
            List<Book> books = CSVHelper.csvToBooks(file.getInputStream());
            return bookRepository.saveAll(books);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }

    public List<Book> findAll() {
        return bookRepository.findAll();
    }
}
