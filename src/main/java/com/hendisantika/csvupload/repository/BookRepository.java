package com.hendisantika.csvupload.repository;

import com.hendisantika.csvupload.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-csv-upload
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/06/21
 * Time: 11.46
 */
public interface BookRepository extends JpaRepository<Book, Long> {
}
