package com.hendisantika.csvupload.controller;

import com.hendisantika.csvupload.dto.ResponseData;
import com.hendisantika.csvupload.entity.Book;
import com.hendisantika.csvupload.helper.CSVHelper;
import com.hendisantika.csvupload.service.BookService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-csv-upload
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/06/21
 * Time: 11.51
 */
@Log4j2
@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    public ResponseEntity<?> findAllBook() {
        ResponseData response = new ResponseData();
        try {
            List<Book> books = bookService.findAll();
            response.setStatus(true);
            response.getMessages().add("Get all books");
            response.setPayload(books);
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setStatus(false);
            response.getMessages().add("Could not get books: " + ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        ResponseData response = new ResponseData();

        if (!CSVHelper.hasCSVFormat(file)) {
            response.setStatus(false);
            response.getMessages().add("Please upload a CSV File");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        try {
            List<Book> books = bookService.save(file);
            response.setStatus(true);
            response.getMessages().add("Uploaded the file successfully: " + file.getOriginalFilename());
            response.setPayload(books);
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            response.setStatus(false);
            response.getMessages().add("Could not upload the file: " + file.getOriginalFilename());
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(response);
        }
    }

}
